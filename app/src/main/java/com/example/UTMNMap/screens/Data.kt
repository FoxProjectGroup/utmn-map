package com.example.UTMNMap.screens

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Menu
import androidx.compose.ui.graphics.vector.ImageVector

fun generateTask(): MutableList<Task> {
    val tasksList = mutableListOf<Task>()
    tasksList.add(
        Task(
            label = "Расписание",
            description = "Идите домой",
            icon = Icons.Filled.LocationOn,
            buttonDescription = "Ехать"
        )
    )
    tasksList.add(
        Task(
            label = "Расписание",
            description = "Идите домой",
            icon = Icons.Filled.LocationOn,
            buttonDescription = "Ехать"
        )
    )
    tasksList.add(
        Task(
            label = "Расписание",
            description = "Идите домой",
            icon = Icons.Filled.LocationOn,
            buttonDescription = "Ехать"
        )
    )
    tasksList.add(
        Task(
            label = "Карта",
            description = "Поиск прохода к кабинету",
            icon = Icons.Filled.LocationOn,
            buttonDescription = "Перейти"
        )
    )
    tasksList.add(
        Task(
            label = "Расписание",
            description = "Идитедомой",
            icon = Icons.Filled.LocationOn,
            buttonDescription = "Ехать"
        )
    )
    tasksList.add(
        Task(
            label = "Расписание",
            description = "Идите домой",
            icon = Icons.Filled.LocationOn,
            buttonDescription = "Ехать"
        )
    )
    tasksList.add(
        Task(
            label = "Карта",
            description = "Поиск прохода к кабинету",
            icon = Icons.Filled.LocationOn,
            buttonDescription = "Перейти"
        )
    )
    tasksList.add(
        Task(
            label = "Карта",
            description = "Поиск прохода к кабинету",
            icon = Icons.Filled.LocationOn,
            buttonDescription = "Перейти"
        )
    )
    return tasksList
}

fun generateTimeTableWeek(): TimeTableWeek {
    val days = mutableListOf<TimeTableDay>()
    val lessons = listOf(
        listOf("Сети и системы передачи информации", "Дед (Захаров А.А.)", "111", "317"),
        listOf(
            "Организация электронно-вычислительных машин и вычислительных систем",
            "Бабич Андрей Владимирович",
            "111",
            "422-1"
        ),
        listOf("Технологии и методы программирования", "Душнила Андрей Валерьевич", "403", "Teams"),
        listOf("Высшая математика", "Зубова Елена Александровна", "111", "421")
    )
    val day = listOf("Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс")
    for (i in 14..20) {
        val less = mutableListOf<Lesson>()
        val startTimes = listOf("8:30", "10:15", "12:00", "14:00", "15:45", "17:30", "19:10")
        val endTimes = listOf("10:00", "11:45", "13:30", "15:30", "17:15", "19:00", "20:40")
        for (j in 0 until (3..7).random()) {
            val lesson = lessons[(lessons.indices).random()]
            val type = (0..1).random()
            less.add(
                Lesson(
                    startTime = startTimes[j],
                    endTime = endTimes[j],
                    teachers = listOf(lesson[1]),
                    title = lesson[0],
                    date = i.toString(),
                    day = day[i - 14],
                    auditorium = lesson[2 + type],
                    type = listOf("Lection", "Practic", "Laboratory").random()
                )
            )
        }
        days.add(TimeTableDay(less, i.toString(), day[i - 14]))
    }
    return TimeTableWeek(
        startDate = days[0].Date,
        endDate = days[days.size - 1].Date,
        mounth = "нояб.",
        year = "2022",
        days = days
    )
}

class Lesson(
    startTime: String = "8:30",
    endTime: String = "10:00",
    teachers: List<String> = listOf("Дед"),
    title: String = "Компьютерные сети",
    date: String = "",
    day: String = "Ср",
    auditorium: String = "403",
    building: String = "УЛК-05",
    capacity: String = "102",
    occupancy: String = "102",
    students: List<String> = listOf(),
    type: String = "Lecture"
) {
    var StartTime: String = "8:30"
    var EndTime: String = "10:00"
    var Teachers: List<String> = listOf()
    var Title: String = ""
    var Date: String = ""
    var Day: String = "Ср"
    var Auditorium: String = "403"
    var Building: String = "УЛК-05"
    var Capacity: String = "102"
    var Occupancy: String = "102"
    var Students: List<String> = listOf()
    var Type: String = "Lecture"

    init {
        StartTime = startTime
        EndTime = endTime
        Teachers = teachers
        Title = title
        Date = date
        Day = day
        Auditorium = auditorium
        Building = building
        Capacity = capacity
        Occupancy = occupancy
        Students = students
        Type = type
    }
}

class Task(label: String, description: String, icon: ImageVector, buttonDescription: String) {
    var Label: String = ""
    var Description: String = ""
    var ButtonDescription: String = ""
    var Icon: ImageVector = Icons.Filled.Menu

    init {
        Label = label
        Description = description
        Icon = icon
        ButtonDescription = buttonDescription
    }
}

class TimeTableWeek(
    startDate: String = "",
    endDate: String = "",
    mounth: String = "",
    year: String = "",
    days: List<TimeTableDay> = listOf()
) {
    var StartDate: String = ""
    var EndDate: String = ""
    var Mounth: String = ""
    var Year: String = ""
    var Days: List<TimeTableDay> = listOf()

    init {
        StartDate = startDate
        EndDate = endDate
        Mounth = mounth
        Year = year
        Days = days
    }
}

class TimeTableDay(lessons: List<Lesson>, date: String = "", day: String = "") {
    var Lessons: List<Lesson> = listOf()
    var Date: String = ""
    var Day: String = "Ср"

    init {
        Lessons = lessons
        Date = date
        Day = day
    }
}