@file:OptIn(ExperimentalUnitApi::class)

package com.example.UTMNMap.screens

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.UTMNMap.R
//import com.example.UTMNMap.ui.theme.Laboratory
import com.example.UTMNMap.ui.theme.MaketsTheme

//
//fun get_less(): List<TimetableElement> {
////    fbf933a5-cae5-44b9-81b4-1dd28eb16a8b
//    val timeTableApi = TimetableApi()
//    return timeTableApi.getPersonTimetable(
//        UUID.fromString("fbf933a5-cae5-44b9-81b4-1dd28eb16a8b"),
//        Date(1670266800),
//        Date(1670353200)
//    )
//
//}

@Composable
fun ModeusCard() {
    var expanded by remember {
        mutableStateOf(false)
    }
    Card(
        modifier = Modifier
            .heightIn(max = 150.dp)
            .fillMaxWidth()
//            .clickable { expanded = !expanded }
            .padding(all = 4.dp),

        shape = MaterialTheme.shapes.medium
    )
    {
        Column(
            modifier = Modifier
                .padding(5.dp)
        ) {
            Row {
                Text(
                    text = "${10}}:${15} - ${11}:${45}",
                    color = MaterialTheme.colorScheme.secondary,
                    style = MaterialTheme.typography.labelSmall,
                    modifier = Modifier
                        .padding(2.dp)
                )
                Spacer(modifier = Modifier.width(4.dp))
                Card(
                    shape = MaterialTheme.shapes.extraSmall
                ) {
                    Text(
//                        text = if (lesson.Type == "Lection") "Лекционное занятие"
//                        else if (lesson.Type == "Practic") "Практическое занятие"
//                        else "Лабораторное занятие",
                        text = "Лекция",
                        color = MaterialTheme.colorScheme.secondary,
                        style = MaterialTheme.typography.labelSmall,
                        modifier = Modifier
                            .background(
                                color = Color.Magenta
//                                color = if (lesson.Type == "Lection") Lection
//                                else if (lesson.Type == "Practic") Practic
//                                else Laboratory
                            )
                            .padding(2.dp)
                    )
                }
            }
            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = "lesson.Title",
                color = MaterialTheme.colorScheme.secondary,
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier
                    .padding(2.dp)
            )
            Spacer(modifier = Modifier.height(4.dp))
            var prepods = ""
            for (i in "lesson.Teachers") {
                prepods += "$i, "
            }
            Text(
                text = prepods,
                color = MaterialTheme.colorScheme.secondary,
                style = MaterialTheme.typography.bodySmall,
                modifier = Modifier
                    .padding(2.dp)
            )
            Spacer(modifier = Modifier.height(4.dp))
            Row {
                Row {
                    Icon(Icons.Filled.LocationOn, "")
                }
                Text(
                    text = "teams",
//                    text = if (lesson.Auditorium != "Teams")
//                            "${lesson.Building}/ ${lesson.Auditorium} (${lesson.Building})"
//                        else "Teams",
                    modifier = Modifier
                        .padding(all = 4.dp),
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }
    }
}

@ExperimentalMaterial3Api
@Composable
fun Modeus(TimeTable: TimeTableWeek, paddingValues: PaddingValues) {
    val navController: NavHostController = rememberNavController()
    Column(modifier = Modifier.fillMaxSize()) {
        ModeusHead(TimeTable, navController)
        NavHost(
            navController = navController,
            startDestination = "Пн"
        ) {
            for (i in TimeTable.Days) {
                composable(i.Day) {
                    LazyColumn(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(Color(0x9900AEEF))
                            .padding(paddingValues)
                            .padding(top = 10.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        items(arrayOf(0)) {
                            ModeusCard()
                        }
                    }
                }
            }
        }

    }
}

@Composable
fun ModeusHeadWeekButton(
    TimeTable: TimeTableDay,
    width: Float,
    navHostController: NavHostController
) {

    Column(
        modifier = Modifier
//                        .fillMaxWidth(0.14f)
            .padding(4.dp)
            .fillMaxHeight(),
        horizontalAlignment = Alignment.CenterHorizontally,
//                    verticalArrangement = Arrangement.Center,
    ) {
        Box(
            modifier = Modifier
                .border(1.dp, Color.Gray, shape = RoundedCornerShape(8.dp))
                .height(45.dp)
                .fillMaxWidth(width)
                .clickable {
                    navHostController.navigate(TimeTable.Day)
                }
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.5f),
                ) {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight()
                    ) {
                        Text(
                            text = TimeTable.Date,
                            modifier = Modifier.align(Alignment.Center),
                            fontSize = 14.sp
                        )
                    }
                }

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(1f),
                ) {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight()
                    ) {
                        Text(
                            text = TimeTable.Day,
                            modifier = Modifier.align(Alignment.Center),
                            fontSize = 14.sp
                        )
                    }
                }
            }

        }
    }

}

@Composable
fun ModeusHead(TimeTable: TimeTableWeek, navHostController: NavHostController) {
    Card(
        modifier = Modifier
            .height(120.dp)
            .fillMaxWidth(),
        shape = RoundedCornerShape(
            topStart = 0.dp,
            topEnd = 0.dp,
            bottomEnd = 0.dp,
            bottomStart = 0.dp,
        )
    ) {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 5.dp, end = 5.dp, bottom = 9.dp, top = 10.dp),
            verticalArrangement = Arrangement.Center
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.43f)
                    .padding(bottom = 5.dp)
//                    .background(Color.Gray)
            ) {

                Column(
                    modifier = Modifier
                        .fillMaxWidth(0.05f)
                        .fillMaxHeight()
                ) {}

                Column(
                    modifier = Modifier
                        .fillMaxWidth(0.14f)
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center,
                ) {
                    IconButton(
                        onClick = { },
                        modifier = Modifier
                            .border(1.dp, Color.Gray, shape = RoundedCornerShape(8.dp))
                            .size(38.dp)
                    ) {
                        Icon(
                            painterResource(id = R.drawable.icon_back),
                            contentDescription = "content description",
                            tint = Color.Gray,
                            modifier = Modifier.size(35.dp)
                        )
                    }
                }

                Column(
                    modifier = Modifier
                        .fillMaxWidth(0.65f)
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center,
                ) {
                    Box(
                        modifier = Modifier
                            .border(1.dp, Color.Gray, shape = RoundedCornerShape(8.dp))
                            .fillMaxWidth()
                            .fillMaxHeight(0.85f)
                    ) {
                        Text(
                            text = "${TimeTable.StartDate} - ${TimeTable.EndDate} ${TimeTable.Mounth} ${TimeTable.Year}",
                            modifier = Modifier
                                .align(Alignment.Center)
                        )
                    }
                }

                Column(
                    modifier = Modifier

                        .fillMaxWidth(0.46f)
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center,
                ) {
                    IconButton(
                        onClick = { },
                        modifier = Modifier
                            .border(1.dp, Color.Gray, shape = RoundedCornerShape(8.dp))
                            .size(38.dp)
                    ) {
                        Icon(
                            painterResource(id = R.drawable.icon_next),
                            contentDescription = "content description",
                            tint = Color.Gray,
                            modifier = Modifier.size(35.dp)
                        )
                    }
                }

                Spacer(modifier = Modifier.width(2.dp))

                Column(
                    modifier = Modifier

                        .fillMaxWidth(0.7f)
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center,
                ) {
                    IconButton(
                        onClick = { },
                        modifier = Modifier
                            .border(1.dp, Color.Gray, shape = RoundedCornerShape(8.dp))
                            .size(38.dp)
                    ) {
                        Icon(
                            painterResource(id = R.drawable.icon_filter),
                            contentDescription = "content description",
                            tint = Color.Gray,
                            modifier = Modifier.size(25.dp)
                        )
                    }
                }

            }


            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .padding(top = 5.dp),

                horizontalArrangement = Arrangement.SpaceEvenly

            ) {
                for (i in 0 until TimeTable.Days.size) {
                    val width = 0.85f / (TimeTable.Days.size - i)
                    ModeusHeadWeekButton(
                        TimeTable = TimeTable.Days[i],
                        width = width,
                        navHostController = navHostController
                    )
                }
            }

        }
    }
}

@Preview(
    name = "Light Mode",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    showBackground = true,
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode"
)

@Composable
@ExperimentalMaterial3Api
fun DefaultPreview1() {
    MaketsTheme {
        var selectedItem by remember { mutableStateOf(0) }
        val icons = listOf("Главная", "Навигация", "Modeus", "FAQ", "Настройки")
        Scaffold(
            bottomBar = {
                NavigationBar(modifier = Modifier.height(60.dp)) {
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_home),
                                contentDescription = "Главная",
                                modifier = Modifier.size(25.dp),

                                )
                        },
                        selected = selectedItem == 0,
                        onClick = { selectedItem = 0 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_modeus),
                                contentDescription = "Модеус",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 1,
                        onClick = { selectedItem = 1 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_gis),
                                contentDescription = "Навигация",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 2,
                        onClick = { selectedItem = 2 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_faq),
                                contentDescription = "FAQ",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 3,
                        onClick = { selectedItem = 3 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_settings),
                                contentDescription = "Настройки",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 4,
                        onClick = { selectedItem = 4 }
                    )
                }
            }
        ) { p ->
            Modeus(generateTimeTableWeek(), p)
        }
    }
}

@Preview
@Composable
fun PreviewCard() {
    ModeusCard()
}