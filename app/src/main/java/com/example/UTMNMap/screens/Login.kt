package com.example.UTMNMap.screens

import android.content.Context
import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.UTMNMap.R
import com.example.UTMNMap.ui.theme.MaketsTheme
import ru.stk.storagedriver.LoginCredentials
import ru.stk.storagedriver.Storage
import utmn.modeus.org.Credentials

@ExperimentalMaterial3Api
@Composable
fun Login(p: PaddingValues, navController: NavController?, onLogin: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0x9900AEEF))
            .padding(p)
            .padding(bottom = 80.dp, start = 40.dp, end = 30.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        LoginCard(navController, onLogin)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginCard(navController: NavController?, onLogin: () -> Unit) {

    var email by remember {
        mutableStateOf("")
    }

    var password by remember {
        mutableStateOf("")
    }

    Column {
        Text(
            text = "Вход",
            textAlign = TextAlign.Left,
            fontSize = 25.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .fillMaxWidth()
        )

        OutlinedTextField(
            value = email,
            onValueChange = { email = it },
            label = { Text(text = "Введите свой email") },
            leadingIcon = {
                Icon(Icons.Default.Person, contentDescription = "person")
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 10.dp)
        )

        OutlinedTextField(
            value = password,
            onValueChange = { password = it },
            label = { Text(text = "Введите свой пароль") },
            leadingIcon = {
                Icon(
                    painterResource(id = R.drawable.icon_password),
                    contentDescription = "person"
                )
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 10.dp),
            visualTransformation = PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)

        )
        val context = LocalContext.current
        OutlinedButton(
            onClick = {
                logged(email, password, context = context)
                onLogin()
                navController?.navigate("dashboard")
            }
        ) {
            Text(
                text = "Войти",
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold
            )
        }
    }
}

fun logged(email: String, password: String, context: Context): Boolean {
    val token = Credentials.login(email, password)
    val loginCredentials = LoginCredentials(login = email, password = password)
    val storage = Storage(context = context, "modeus")
    storage.saveLogin(login = loginCredentials)
    storage.saveExtra(custom = token)
    return token != ""
}

@Preview(
    name = "Light Mode",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    showBackground = true
)
@Preview(
    name = "Dark Mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true
)

@Composable
@ExperimentalMaterial3Api
fun DefaultPreview2() {
    MaketsTheme {

        var selectedItem by remember { mutableStateOf(0) }
        val icons = listOf("Главная", "Навигация", "Modeus", "FAQ", "Настройки")
        Scaffold(
            bottomBar = {
                NavigationBar(modifier = Modifier.height(60.dp)) {
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_home),
                                contentDescription = "Главная",
                                modifier = Modifier.size(25.dp),

                                )
                        },
                        selected = selectedItem == 0,
                        onClick = { selectedItem = 0 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_modeus),
                                contentDescription = "Модеус",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 1,
                        onClick = { selectedItem = 1 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_gis),
                                contentDescription = "Навигация",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 2,
                        onClick = { selectedItem = 2 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_faq),
                                contentDescription = "FAQ",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 3,
                        onClick = { selectedItem = 3 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_settings),
                                contentDescription = "Настройки",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 4,
                        onClick = { selectedItem = 4 }
                    )
                }
            }
        ) { p ->
            Login(p, null, onLogin = {})
        }
    }
}
