package com.example.UTMNMap.screens

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.UTMNMap.R
import com.example.UTMNMap.ui.theme.MaketsTheme
import ru.stk.storagedriver.Storage

@ExperimentalMaterial3Api
@Composable
fun Settings(p: PaddingValues, navController: NavController?, onLoginChange: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0x9900AEEF))
            .padding(p)
            .padding(top = 10.dp)
    ) {
        SettingsCard(navController, onLoginChange)
    }
}

@Composable
fun SettingsCard(navController: NavController?, onLoginChange: () -> Unit) {
    val storage = Storage(context = LocalContext.current, "modeus")
    OutlinedButton(
        onClick = {
            storage.clearLogin()
            onLoginChange()
            navController?.navigate("login")
        }
    ) {
        Text(
            text = "Выйти",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold
        )
    }

}


@Preview(
    name = "Light Mode",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    showBackground = true
)
@Preview(
    name = "Dark Mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true
)

@Composable
@ExperimentalMaterial3Api
fun DefaultPreview3() {
    MaketsTheme {

        var selectedItem by remember { mutableStateOf(0) }
//        val icons = listOf("Главная", "Навигация", "Modeus", "FAQ", "Настройки")
        Scaffold(
            bottomBar = {
                NavigationBar(modifier = Modifier.height(60.dp)) {
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_home),
                                contentDescription = "Главная",
                                modifier = Modifier.size(25.dp),

                                )
                        },
                        selected = selectedItem == 0,
                        onClick = { selectedItem = 0 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_modeus),
                                contentDescription = "Модеус",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 1,
                        onClick = { selectedItem = 1 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_gis),
                                contentDescription = "Навигация",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 2,
                        onClick = { selectedItem = 2 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_faq),
                                contentDescription = "FAQ",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 3,
                        onClick = { selectedItem = 3 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_settings),
                                contentDescription = "Настройки",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 4,
                        onClick = { selectedItem = 4 }
                    )
                }
            }
        ) { p ->
            Settings(p, navController = null, onLoginChange = {})
        }
    }
}
