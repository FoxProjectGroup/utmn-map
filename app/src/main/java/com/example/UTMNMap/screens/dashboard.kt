package com.example.UTMNMap.screens

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.UTMNMap.MainActivity
import com.example.UTMNMap.R
import com.example.UTMNMap.ui.theme.MaketsTheme

@Composable
fun Dashboard(tasks: List<Task>, p: PaddingValues) {
    MaketsTheme {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.background)
                .padding(p)
                .padding(top = 10.dp),
//            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            items(tasks) { task ->
                DashboardCard(task = task)
            }
        }
    }

}

@Composable
fun DashboardCard(task: Task) {
    MaketsTheme {
        Card(
            modifier = Modifier
                .heightIn(max = 150.dp)
//            .fillMaxSize()
                .padding(all = 4.dp),

            shape = MaterialTheme.shapes.medium,
            colors = CardDefaults.cardColors(
                MaterialTheme.colorScheme.secondaryContainer
            )
        ) {

            Column(
                modifier = Modifier
                    .heightIn(max = 150.dp)
                    .fillMaxSize()
//                .padding(all = 4.dp)
                    .padding(horizontal = 10.dp, vertical = 4.dp)

            ) {
                Text(
                    text = task.Label,
                    color = MaterialTheme.colorScheme.secondary,
                    style = MaterialTheme.typography.headlineSmall
                )
                Spacer(modifier = Modifier.height(4.dp))


                Row(
                    modifier = Modifier.fillMaxSize(),
//                horizontalArrangement = Arrangement.End
                ) {
                    Text(
                        text = task.Description,
                        modifier = Modifier
                            .padding(all = 4.dp)
                            .fillMaxWidth(0.85f),
                        style = MaterialTheme.typography.bodyMedium
                    )
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 10.dp, start = 10.dp),
                        horizontalArrangement = Arrangement.End,
                        verticalAlignment = Alignment.Bottom
                    ) {
                        IconButton(
                            onClick = {},
                            modifier = Modifier.fillMaxSize()
                        ) {
                            Icon(task.Icon, task.ButtonDescription)
                        }
                    }
                }
            }
        }
    }


}


@Preview(
    name = "Light Mode",
    uiMode = Configuration.UI_MODE_TYPE_NORMAL,
    showBackground = true,
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode"
)
@Composable
fun DefaultPreview() {
    MaketsTheme {

        val NavBarsColors = NavigationBarItemDefaults.colors(
            MaterialTheme.colorScheme.onPrimaryContainer,
            MaterialTheme.colorScheme.onSurface,
            MaterialTheme.colorScheme.primaryContainer,
            MaterialTheme.colorScheme.onSurfaceVariant,
            MaterialTheme.colorScheme.onSurfaceVariant,
            MaterialTheme.colorScheme.onSurfaceVariant.copy(alpha = MainActivity.DisabledAlpha),
            MaterialTheme.colorScheme.onSurfaceVariant.copy(alpha = MainActivity.DisabledAlpha),
        )
        var selectedItem by remember { mutableStateOf(0) }
//        val icons = listOf("Главная", "Навигация", "Modeus", "FAQ", "Настройки")
        Scaffold(
            bottomBar = {
                NavigationBar(
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    contentColor = MaterialTheme.colorScheme.contentColorFor(MaterialTheme.colorScheme.primaryContainer),
                    modifier = Modifier.height(60.dp)
                ) {
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_home),
                                contentDescription = "Главная",
                                modifier = Modifier.size(25.dp),

                                )
                        },
                        selected = selectedItem == 0,
                        onClick = { selectedItem = 0 },
                        colors = NavBarsColors
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_modeus),
                                contentDescription = "Модеус",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 1,
                        onClick = { selectedItem = 1 },
                        colors = NavBarsColors
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_gis),
                                contentDescription = "Навигация",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 2,
                        onClick = { selectedItem = 2 },
                        colors = NavBarsColors
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_faq),
                                contentDescription = "FAQ",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 3,
                        onClick = { selectedItem = 3 },
                        colors = NavBarsColors
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_settings),
                                contentDescription = "Настройки",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 4,
                        onClick = { selectedItem = 4 },
                        colors = NavBarsColors
                    )
                }
            }
        ) { p ->
            val tasksList = mutableListOf<Task>()
            tasksList.add(
                Task(
                    label = "Расписание",
                    description = "Идите домой",
                    icon = Icons.Filled.LocationOn,
                    buttonDescription = "Ехать"
                )
            )
            tasksList.add(
                Task(
                    label = "Расписание",
                    description = "Идите домой",
                    icon = Icons.Filled.LocationOn,
                    buttonDescription = "Ехать"
                )
            )
            tasksList.add(
                Task(
                    label = "Расписание",
                    description = "Идите домой",
                    icon = Icons.Filled.LocationOn,
                    buttonDescription = "Ехать"
                )
            )
            tasksList.add(
                Task(
                    label = "Карта",
                    description = "Поиск прохода к кабинету",
                    icon = Icons.Filled.LocationOn,
                    buttonDescription = "Перейти"
                )
            )
            tasksList.add(
                Task(
                    label = "Расписание",
                    description = "Идитедомой",
                    icon = Icons.Filled.LocationOn,
                    buttonDescription = "Ехать"
                )
            )
            tasksList.add(
                Task(
                    label = "Расписание",
                    description = "Идите домой",
                    icon = Icons.Filled.LocationOn,
                    buttonDescription = "Ехать"
                )
            )
            tasksList.add(
                Task(
                    label = "Карта",
                    description = "Поиск прохода к кабинету",
                    icon = Icons.Filled.LocationOn,
                    buttonDescription = "Перейти"
                )
            )
            tasksList.add(
                Task(
                    label = "Карта",
                    description = "Поиск прохода к кабинету",
                    icon = Icons.Filled.LocationOn,
                    buttonDescription = "Перейти"
                )
            )
            Dashboard(tasksList, p)
        }
    }
}