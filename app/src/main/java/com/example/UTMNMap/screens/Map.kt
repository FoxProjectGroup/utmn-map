package com.example.UTMNMap.screens

//import com.mapbox.common.LifecycleMonitorAndroid.Companion.getInstance
import DataGrid
import FeatureCollections
import Graph
import Line
import Piece
import PolyShape
import android.content.Context
import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.UTMNMap.R
import com.example.UTMNMap.graph.IGiP
import com.example.UTMNMap.graph.Zdanie
import com.example.UTMNMap.ui.theme.MaketsTheme
import com.google.gson.Gson
import com.mapbox.geojson.Feature
import com.mapbox.geojson.LineString
import com.mapbox.geojson.MultiLineString
import com.mapbox.geojson.Point
import com.mapbox.maps.extension.style.expressions.dsl.generated.get
import java.io.InputStream


const val coef = 1000000
@ExperimentalMaterial3Api
@Composable
fun Maps(p: PaddingValues) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0x9900AEEF))
            .padding(p)
            .padding(top = 0.dp)
    ) {
        var punkt1 by remember {
            mutableStateOf(0)
        }
        val thisContext = LocalContext.current
        var punkt2 by remember {
            mutableStateOf(0)
        }

        var punkt1_e by remember {
            mutableStateOf(0)
        }
        var punkt2_e by remember {
            mutableStateOf(0)
        }

        var mExpanded by remember { mutableStateOf(false) }

        val mCities = listOf("ИГиП")
        val mList = listOf(IGiP)
        var zdanie by remember {
            mutableStateOf(mList[0])
        }
        var number by remember {
            mutableStateOf(mList[0].start_ind)
        }

        Column {
            var selectedOptionText by remember { mutableStateOf(mCities[0]) }
            ExposedDropdownMenuBox(
                expanded = mExpanded,
                onExpandedChange = { mExpanded = !mExpanded },
                modifier = Modifier.fillMaxWidth()
            ) {
                TextField(
                    // The `menuAnchor` modifier must be passed to the text field for correctness.
                    modifier = Modifier
                        .menuAnchor()
                        .fillMaxWidth(),
                    readOnly = true,
                    value = "$selectedOptionText $number",
                    onValueChange = {},
                    label = { Text("Корпус и этаж") },
                    trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = mExpanded) },
                    colors = ExposedDropdownMenuDefaults.textFieldColors(),
                )
                ExposedDropdownMenu(
                    expanded = mExpanded,
                    onDismissRequest = { mExpanded = false },
                    modifier = Modifier.fillMaxWidth()
                ) {
                    (mCities.indices).forEach { i ->
                        val selectionOption = mCities[i]
                        (0 until mList[i].kabinet.size).forEach {
                            DropdownMenuItem(
                                text = { Text("$selectionOption $it") },
                                onClick = {
                                    selectedOptionText = selectionOption
                                    zdanie = mList[i]
                                    number = it
                                    mExpanded = false
                                },
                                contentPadding = ExposedDropdownMenuDefaults.ItemContentPadding
                            )
                        }
                    }
                }
            }

            Row {
                var startEExpanded by remember { mutableStateOf(false) }
                var startEExpanded_ by remember { mutableStateOf(false) }
                var selectedStartE by remember { mutableStateOf(zdanie.start_ind) }
                ExposedDropdownMenuBox(
                    expanded = startEExpanded,
                    onExpandedChange = { startEExpanded = !startEExpanded }
                ) {
                    TextField(
                        // The `menuAnchor` modifier must be passed to the text field for correctness.
                        modifier = Modifier
                            .menuAnchor()
                            .fillMaxWidth(0.3f),
                        readOnly = true,
                        value = selectedStartE.toString(),
                        onValueChange = {},
                        label = { Text("Этаж") },
                        trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = startEExpanded) },
                        colors = ExposedDropdownMenuDefaults.textFieldColors(),
                    )
                    ExposedDropdownMenu(
                        expanded = startEExpanded,
                        onDismissRequest = { startEExpanded = false }
                    ) {
                        (0 until zdanie.kabinet.size).forEach { selectionOption ->
                            DropdownMenuItem(
                                text = { Text((selectionOption + zdanie.start_ind).toString()) },
                                onClick = {
                                    selectedStartE = selectionOption
                                    startEExpanded = false
                                    punkt1_e = selectedStartE
                                },
                                contentPadding = ExposedDropdownMenuDefaults.ItemContentPadding
                            )

                        }
                    }
                }
                ExposedDropdownMenuBox(
                    expanded = startEExpanded_,
                    onExpandedChange = { startEExpanded_ = !startEExpanded_ }
                ) {
                    TextField(
                        // The `menuAnchor` modifier must be passed to the text field for correctness.
                        modifier = Modifier
                            .menuAnchor()
                            .fillMaxWidth(),
                        readOnly = true,
                        value = zdanie.kabinet[selectedStartE][0][punkt1],
                        onValueChange = {},
                        label = { Text("А") },
                        trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = startEExpanded_) },
                        colors = ExposedDropdownMenuDefaults.textFieldColors(),
                    )
                    ExposedDropdownMenu(
                        expanded = startEExpanded_,
                        onDismissRequest = { startEExpanded_ = false }
                    ) {
                        (0 until zdanie.kabinet[selectedStartE][0].size).forEach { selectionOption ->
                            DropdownMenuItem(
                                text = { Text(zdanie.kabinet[selectedStartE][0][selectionOption]) },
                                onClick = {
                                    punkt1 = selectionOption
                                    startEExpanded_ = false
                                },
                                contentPadding = ExposedDropdownMenuDefaults.ItemContentPadding
                            )
                        }
                    }
                }
            }
            Row {
                var startEExpanded by remember { mutableStateOf(false) }
                var startEExpanded_ by remember { mutableStateOf(false) }
                var selectedStartE by remember { mutableStateOf(zdanie.start_ind) }
                ExposedDropdownMenuBox(
                    expanded = startEExpanded,
                    onExpandedChange = { startEExpanded = !startEExpanded }
                ) {
                    TextField(
                        // The `menuAnchor` modifier must be passed to the text field for correctness.
                        modifier = Modifier
                            .menuAnchor()
                            .fillMaxWidth(0.3f),
                        readOnly = true,
                        value = selectedStartE.toString(),
                        onValueChange = {},
                        label = { Text("Этаж") },
                        trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = startEExpanded) },
                        colors = ExposedDropdownMenuDefaults.textFieldColors(),
                    )
                    ExposedDropdownMenu(
                        expanded = startEExpanded,
                        onDismissRequest = { startEExpanded = false }
                    ) {
                        (0 until zdanie.kabinet.size).forEach { selectionOption ->
                            DropdownMenuItem(
                                text = { Text((selectionOption + zdanie.start_ind).toString()) },
                                onClick = {
                                    selectedStartE = selectionOption
                                    punkt2_e = selectedStartE
                                    startEExpanded = false
                                },
                                contentPadding = ExposedDropdownMenuDefaults.ItemContentPadding
                            )

                        }
                    }
                }
                ExposedDropdownMenuBox(
                    expanded = startEExpanded_,
                    onExpandedChange = { startEExpanded_ = !startEExpanded_ }
                ) {
                    TextField(
                        // The `menuAnchor` modifier must be passed to the text field for correctness.
                        modifier = Modifier
                            .menuAnchor()
                            .fillMaxWidth(),
                        readOnly = true,
                        value = zdanie.kabinet[selectedStartE][0][punkt2],
                        onValueChange = {},
                        label = { Text("Б") },
                        trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = startEExpanded_) },
                        colors = ExposedDropdownMenuDefaults.textFieldColors(),
                    )
                    ExposedDropdownMenu(
                        expanded = startEExpanded_,
                        onDismissRequest = { startEExpanded_ = false }
                    ) {
                        (0 until zdanie.kabinet[selectedStartE][0].size).forEach { selectionOption ->
                            DropdownMenuItem(
                                text = { Text(zdanie.kabinet[selectedStartE][0][selectionOption]) },
                                onClick = {
                                    punkt2 = selectionOption
                                    startEExpanded_ = false
                                },
                                contentPadding = ExposedDropdownMenuDefaults.ItemContentPadding
                            )
                        }
                    }
                }
            }
        }
//        AndroidView(
//            modifier = Modifier
//                .fillMaxSize(),
//            factory = { context ->
//                ResourceOptionsManager.getDefault(
//                    context,
//                    context.getString(R.string.mapbox_access_token)
//                )
//                MapView(context).apply {
//
//                    getMapboxMap().loadStyle(
//                        style(styleUri = Style.LIGHT) {
//                            +geoJsonSource("id") {
//                                url("asset://${zdanie.fileName}_$number.geojson")
//                            }
//                            +geoJsonSource("lines") {
//                                feature(getLineGraph(thisContext, "${zdanie.fileName}_$number"))
//                            }
//                            +fillLayer("room-extrusion", "id") {
//                                fillColor(get("color"))
//                                fillOpacity(0.5)
//                            }
//                            +lineLayer("linelayer", "lines") {
//                                lineCap(LineCap.ROUND)
//                                lineJoin(LineJoin.ROUND)
//                                lineOpacity(0.7)
//                                lineWidth(1.0)
//                                lineColor("#ff0f0f")
//                            }
//                        }
//                    )
//                    getMapboxMap().setCamera(
//                        CameraOptions.Builder().center(
//                            Point.fromLngLat(
//                                0.006, 0.001
//                            )
//                        ).zoom(14.99).pitch(30.0).bearing(30.0).build()
//                    )
//                }
//            },
//            update = {
//
//                it.getMapboxMap().loadStyle(
//                    style(styleUri = Style.LIGHT) {
//                        +geoJsonSource("id") {
//                            url("asset://${zdanie.fileName}_$number.geojson")
//                        }
//                        +fillLayer("room-extrusion", "id") {
//                            fillColor(get("color"))
//                            fillOpacity(0.5)
//                        }
//                        +geoJsonSource("lines") {
//                            if ((number == 5 || number == 3) && (punkt1_e == 5 || punkt1_e == 3) && (punkt2_e == 5 || punkt2_e == 3))
//                                feature(
//                                    getBfs(
//                                        thisContext,
//                                        zdanie,
//                                        zdanie.kabinet[punkt1_e][1][punkt1],
//                                        zdanie.kabinet[punkt2_e][1][punkt2],
//                                        punkt1_e,
//                                        punkt2_e,
//                                        number
//                                    )
//                                )
//                            else
//                                feature(getLineGraph(thisContext, "${zdanie.fileName}_$number"))
//                        }
//                        +lineLayer("linelayer", "lines") {
//                            lineCap(LineCap.ROUND)
//                            lineJoin(LineJoin.ROUND)
//                            lineOpacity(0.7)
//                            lineWidth(1.0)
//                            lineColor("#ff0f0f")
//                        }
//                    }
//                )
//            }
//        )


        val gson = Gson()
        val context = LocalContext.current
        val inputStream: InputStream = context.assets.open("${zdanie.fileName}_$number.geojson")
        val size: Int = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        val res = gson.fromJson(String(buffer), FeatureCollections::class.java)

        val paths = mutableListOf<Path>() // список для хранения всех путей
//        val texts by remember { mutableStateOf(mutableListOf<String>()) } // список для хранения всех путей
        val colors = mutableListOf<Color>() // список для хранения всех путей
        val features = res.features
        features.forEach { feature ->
//        val color = Color("ac".toInt(16),"e5".toInt(16),"ff".toInt(16))
            var col = feature.properties.color
            val color = Color(
                (col[0].toString() + col[1].toString()).toInt(16),
                (col[2].toString() + col[3].toString()).toInt(16),
                (col[4].toString() + col[5].toString()).toInt(16),
                0xC0
            )
            var path = Path()
            if (feature.geometry.coordinates.isNotEmpty()) {
                if (feature.geometry.coordinates[0].isNotEmpty()) {
                    path.moveTo(
                        feature.geometry.coordinates[0].first()[0].toFloat(),
                        feature.geometry.coordinates[0].first()[1].toFloat()
//                    0f,0f
                    )
                    for (i in 1 until feature.geometry.coordinates[0].size)
                        path.lineTo(
                            feature.geometry.coordinates[0][i][0].toFloat(),
                            feature.geometry.coordinates[0][i][1].toFloat()
                        )
//                path.addRect(Rect(Offset.Zero, Offset(400f, 400f)))
//                path.close()
                    //            val path = Path()
                    //            path.moveTo(200f, 200f)
                    //            path.lineTo(300f, 400f)
                    //            path.lineTo(400f, 200f)
                    //            path.moveTo(200f, 200f)
                    //            path.close()
                    //            }
                    if (color in colors) {
                        val ind = colors.indexOf(color)
                        paths[ind].addPath(path)
                    } else {
                        paths.add(path)
                        colors.add(color)
                    }
                }
            }
        }
        PolyShape(
            paths = paths,
            colors = colors,
            lines = getLineGraph(thisContext, "${zdanie.fileName}_$number")
        )
    }
}

fun getLineGraph(context: Context, zdanie: String): List<Line> {
    val gson = Gson()
    val inputStream: InputStream = context.assets.open("$zdanie.json")
    val size: Int = inputStream.available()
    val buffer = ByteArray(size)
    inputStream.read(buffer)
    val coordinates = mutableListOf<Line>()
    val res = gson.fromJson(String(buffer), DataGrid::class.java)
    for (i in res.lines) {
//        val routeCoordinates = mutableListOf<Piece>()
//        routeCoordinates +=
//        routeCoordinates +=
        coordinates.add(Line(Piece(i.startX, i.startY), Piece(i.endX, i.endY)))
    }
    return coordinates
}

fun getBfs(
    context: Context,
    zdanie: Zdanie,
    start: String,
    end: String,
    start_e: Int,
    end_e: Int,
    selected: Int
): Feature {
    val g = Graph()
    val gson = Gson()
    val inputStream: InputStream = context.assets.open("${zdanie.fileName}_$start_e.json")
    val size: Int = inputStream.available()
    val buffer = ByteArray(size)
    inputStream.read(buffer)
    val res = gson.fromJson(String(buffer), DataGrid::class.java)
    var res1 = DataGrid(listOf(), listOf())
    val coordinates = mutableListOf<LineString>()
    val p = mutableListOf<String>()
    for (i in res.pieces) {
        p.add(i.id)
        g.addVertex(i.id)
    }
    for (i in res.lines) {
        g.connect(i.startId, i.endId)
    }
    if (start_e != end_e) {
        val gson1 = Gson()
        val inputStream1: InputStream = context.assets.open("${zdanie.fileName}_$end_e.json")
        val size1: Int = inputStream1.available()
        val buffer1 = ByteArray(size1)
        inputStream1.read(buffer1)
        res1 = gson1.fromJson(String(buffer1), DataGrid::class.java)
        for (i in res1.pieces) {
            if (i.id in p)
                continue
            p.add(i.id)
            g.addVertex(i.id)
        }
        for (i in res1.lines) {
            g.connect(i.startId, i.endId)
        }
    }
    println(start)
    println(g.debugVertex())
    println(end)
    g.bfs(start, end) // указываем кабинеты
    val pieces = mutableStateListOf<Piece>()
    if (start_e != end_e) {
        if (selected == start_e) {
            for (i in g.line) {
                for (j in res.pieces) {
                    if (j.id == i) {
                        val piece = Piece(j.posX, j.posY)
                        pieces.add(piece)
                    }
                }
                println(i)
                if (i == "Лифт" || i.split(" ")[0] == "Лестница")
                    break
            }
        }
        if (selected == end_e) {
            var flg = false
            for (i in g.line) {
                if (i == "Лифт" || i.split(" ")[0] == "Лестница") {
                    flg = true
                }
                if (!flg) {
                    continue
                }
                for (j in res1.pieces) {
                    if (j.id == i) {
                        val piece = Piece(j.posX, j.posY)
                        pieces.add(piece)
                    }
                }
                println(i)
            }
        }
    } else {
        for (i in g.line) {
            for (j in res.pieces) {
                if (j.id == i) {
                    val piece = Piece(j.posX, j.posY)
                    pieces.add(piece)
                }
            }
            println(i)
        }
    }
    for (i in pieces) {
        println("${i.posX}_${i.posY}")
    }

    for (i in 0 until pieces.size) {
        if (i != pieces.size - 1) {
            print(pieces.size)
            val startPiece = Piece(pieces[i].posX, pieces[i].posY)
            val endPiece = Piece(pieces[i + 1].posX, pieces[i + 1].posY)
            val routeCoordinates = mutableListOf<Point>()
            routeCoordinates.add(Point.fromLngLat(startPiece.posX / coef, startPiece.posY / coef))
            routeCoordinates.add(Point.fromLngLat(endPiece.posX / coef, endPiece.posY / coef))
            coordinates.add(LineString.fromLngLats(routeCoordinates))
        }
    }
    val lines = MultiLineString.fromLineStrings(coordinates)

    return Feature.fromGeometry(lines)
}

//fun getRandomString(length: Int) : String {
//    val allowedChars = ('A'..'Z') + ('a'..'z')
//    return (1..length)
//        .map { allowedChars.random() }
//        .joinToString("")
//}

@Preview(
    name = "Light Mode",
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    showBackground = true
)
@Preview(
    name = "Dark Mode",
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true
)

@Composable
@ExperimentalMaterial3Api
fun DefaultPreview4() {
    MaketsTheme {

        var selectedItem by remember { mutableStateOf(0) }
//        val icons = listOf("Главная", "Навигация", "Modeus", "FAQ", "Настройки")
        Scaffold(
            bottomBar = {
                NavigationBar(modifier = Modifier.height(60.dp)) {
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_home),
                                contentDescription = "Главная",
                                modifier = Modifier.size(25.dp),

                                )
                        },
                        selected = selectedItem == 0,
                        onClick = { selectedItem = 0 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_modeus),
                                contentDescription = "Модеус",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 1,
                        onClick = { selectedItem = 1 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_gis),
                                contentDescription = "Навигация",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 2,
                        onClick = { selectedItem = 2 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_faq),
                                contentDescription = "FAQ",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 3,
                        onClick = { selectedItem = 3 }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_settings),
                                contentDescription = "Настройки",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 4,
                        onClick = { selectedItem = 4 }
                    )
                }
            }
        ) { p ->
            Maps(p)
        }
    }
}
