@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package com.example.UTMNMap

//import ru..storagedriver.Storage

//import com.example.UTMNMap.ui.theme.MaketsTheme
import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.UTMNMap.screens.*
import com.example.UTMNMap.ui.theme.MaketsTheme
import ru.stk.storagedriver.Storage


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaketsTheme {
//            val policy = ThreadPolicy.Builder().permitAll().build()
//                StrictMode.setThreadPolicy(policy)
                val navController: NavHostController = rememberNavController()
                var selectedItem by remember { mutableStateOf(0) }
                val storage = Storage(context = LocalContext.current, "modeus")
                var show_bar by remember {
                    mutableStateOf(storage.getLogin() != null)
                }
                val NavBarsColors = NavigationBarItemDefaults.colors(
                    MaterialTheme.colorScheme.onSecondary,
                    MaterialTheme.colorScheme.onSurface,
                    MaterialTheme.colorScheme.primaryContainer,
                    MaterialTheme.colorScheme.onSurfaceVariant,
                    MaterialTheme.colorScheme.onSurfaceVariant,
                    MaterialTheme.colorScheme.onSurfaceVariant.copy(alpha = DisabledAlpha),
                    MaterialTheme.colorScheme.onSurfaceVariant.copy(alpha = DisabledAlpha),
                )
                val NavBarItemHeight = 25.dp
                val NavBarHeight = 60.dp
//            val icons = listOf("Главная", "Навигация", "Modeus", "FAQ", "Настройки")
                Scaffold(
                    bottomBar = {
                        if (show_bar) {
                            NavigationBar(
                                modifier = Modifier.height(NavBarHeight),
                                containerColor = MaterialTheme.colorScheme.primaryContainer,
                            ) {
                                NavigationBarItem(
                                    icon = {
                                        Icon(
                                            painterResource(id = R.drawable.icon_home),
                                            contentDescription = "Главная",
                                            modifier = Modifier.size(NavBarItemHeight),

                                            )
                                    },
                                    selected = selectedItem == 0,
                                    onClick = {
                                        selectedItem = 0
                                        navController.navigate("dashboard")
                                    },
                                    colors = NavBarsColors
                                )
                                NavigationBarItem(
                                    icon = {
                                        Icon(
                                            painterResource(id = R.drawable.icon_modeus),
                                            contentDescription = "Модеус",
                                            modifier = Modifier.size(NavBarItemHeight),

                                            )
                                    },
                                    selected = selectedItem == 1,
                                    onClick = {
                                        selectedItem = 1
                                        navController.navigate("modeus")
                                    },
                                    colors = NavBarsColors
                                )
                                NavigationBarItem(
                                    icon = {
                                        Icon(
                                            painterResource(id = R.drawable.icon_gis),
                                            contentDescription = "Навигация",
                                            modifier = Modifier.size(NavBarItemHeight),

                                            )
                                    },
                                    selected = selectedItem == 2,
                                    onClick = {
                                        selectedItem = 2
                                        navController.navigate("maps")
                                    },
                                    colors = NavBarsColors
                                )
                                NavigationBarItem(
                                    icon = {
                                        Icon(
                                            painterResource(id = R.drawable.icon_faq),
                                            contentDescription = "FAQ",
                                            modifier = Modifier.size(NavBarItemHeight),

                                            )
                                    },
                                    selected = selectedItem == 3,
                                    onClick = {
                                        selectedItem = 3
                                        navController.navigate("FAQ")
                                    },
                                    colors = NavBarsColors
                                )
                                NavigationBarItem(
                                    icon = {
                                        Icon(
                                            painterResource(id = R.drawable.icon_settings),
                                            contentDescription = "Настройки",
                                            modifier = Modifier.size(NavBarItemHeight),

                                            )
                                    },
                                    selected = selectedItem == 4,
                                    onClick = {
                                        selectedItem = 4
                                        navController.navigate("settings")
                                    },
                                    colors = NavBarsColors
                                )
                            }
                        }
                    },
                    containerColor = MaterialTheme.colorScheme.background,
                ) { p ->
                    Navigate(
                        navController = navController,
                        storage = storage,
                        p = p,
                        onLoginChange = { show_bar = storage.getLogin() != null })
                }
            }
        }
    }

    companion object {
        internal const val DisabledAlpha = 0.38f
    }
}

@Composable
fun Navigate(
    navController: NavHostController,
    storage: Storage,
    p: PaddingValues,
    onLoginChange: () -> Unit
) {

    NavHost(
        navController = navController,
        startDestination = if (storage.getLogin() != null) "dashboard" else "login"
    ) {
        composable("dashboard") {
            Dashboard(tasks = generateTask(), p = p)
        }
        composable("modeus") {
//            Modeus(TimeTable = generateTimeTableWeek(), paddingValues = p)
            val storage = Storage(context = LocalContext.current, "modeus")
            storage.getExtra()?.let { it1 -> Text(it1) }
        }
        composable("FAQ") {
            Wip(painter = painterResource(id = R.drawable.icon_wip), title = "FAQ")
        }
        composable("settings") {
            Settings(p = p, navController = navController, onLoginChange)
        }
        composable("maps") {
//            Wip(painter = painterResource(id = R.drawable.icon_wip), title = "Maps")
            Maps(p = p)
        }
        composable("login") {
            Login(p = p, navController, onLoginChange)
        }
    }
}

@Preview(uiMode = Configuration.UI_MODE_NIGHT_NO or Configuration.UI_MODE_TYPE_NORMAL)
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL)
@Composable
fun DefaultPreview() {
    MaketsTheme {

        val navController: NavHostController = rememberNavController()
        var selectedItem by remember { mutableStateOf(0) }
        val NavBarsColors = NavigationBarItemDefaults.colors(
            MaterialTheme.colorScheme.onSecondaryContainer,
            MaterialTheme.colorScheme.onSurface,
            MaterialTheme.colorScheme.primaryContainer,
            MaterialTheme.colorScheme.onSurfaceVariant,
            MaterialTheme.colorScheme.onSurfaceVariant,
            MaterialTheme.colorScheme.onSurfaceVariant.copy(alpha = MainActivity.DisabledAlpha),
            MaterialTheme.colorScheme.onSurfaceVariant.copy(alpha = MainActivity.DisabledAlpha),
        )
        Scaffold(
            bottomBar = {
                NavigationBar(
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    modifier = Modifier.height(60.dp)
                ) {
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_home),
                                contentDescription = "Главная",
                                modifier = Modifier.size(25.dp),
                            )
                        },
                        selected = selectedItem == 0,
                        onClick = {
                            selectedItem = 0
                            navController.navigate("dashboard")
                        },
                        colors = NavBarsColors
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_modeus),
                                contentDescription = "Модеус",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 1,
                        onClick = {
                            selectedItem = 1
                            navController.navigate("modeus")
                        },
                        colors = NavBarsColors
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_gis),
                                contentDescription = "Навигация",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 2,
                        onClick = {
                            selectedItem = 2
                            navController.navigate("maps")
                        }
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_faq),
                                contentDescription = "FAQ",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 3,
                        onClick = {
                            selectedItem = 3
                            navController.navigate("FAQ")
                        },
                        colors = NavBarsColors
                    )
                    NavigationBarItem(
                        icon = {
                            Icon(
                                painterResource(id = R.drawable.icon_settings),
                                contentDescription = "Настройки",
                                modifier = Modifier.size(25.dp)
                            )
                        },
                        selected = selectedItem == 4,
                        onClick = {
                            selectedItem = 4
                            navController.navigate("settings")
                        },
                        colors = NavBarsColors
                    )
                }
            },
            containerColor = MaterialTheme.colorScheme.background,

            ) { p ->
            NavHost(
                navController = navController,
                startDestination = "dashboard"
            ) {
                composable("dashboard") {
                    Dashboard(tasks = generateTask(), p = p)
                }
                composable("modeus") {
                    Modeus(TimeTable = generateTimeTableWeek(), paddingValues = p)
                }
                composable("FAQ") {
                    Wip(painter = painterResource(id = R.drawable.icon_wip), title = "FAQ")
                }
                composable("settings") {
                    Wip(painter = painterResource(id = R.drawable.icon_wip), title = "Settings")
                }
                composable("maps") {
                    Wip(painter = painterResource(id = R.drawable.icon_wip), title = "Maps")
                }
            }
        }
    }
}