import android.content.res.Configuration
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTransformGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.example.UTMNMap.ui.theme.MaketsTheme
import com.google.gson.Gson
import java.io.InputStream

@Composable
fun PolyShape(paths: List<Path>, colors: List<Color>, lines: List<Line> = listOf()) {
    var scale by remember { mutableStateOf(1f) }
    var rotation by remember { mutableStateOf(0f) }
    var offsetX by remember { mutableStateOf(0f) }
    var offsetY by remember { mutableStateOf(0f) }
    MaketsTheme {
        Box(
            Modifier
                .fillMaxSize()
                .pointerInput(Unit) {
                    detectTransformGestures { centroid, pan, zoom, rotate ->
                        scale *= zoom
                        rotation += rotate
                        offsetX += pan.x
                        offsetY += pan.y
                    }
                }
                .pointerInput(Unit) {
                    detectDragGestures { change, dragAmount ->
                        change.consume()
                        offsetX += dragAmount.x
                        offsetY += dragAmount.y
                    }
                }
                .background(MaterialTheme.colorScheme.background)
        ) {
            var color_lines = MaterialTheme.colorScheme.error
            Canvas(
                Modifier
                    .matchParentSize()
                    .graphicsLayer(
                        scaleX = scale,
                        scaleY = scale,
                        rotationZ = rotation,
                        translationX = offsetX,
                        translationY = offsetY
                    )
            ) {
//                println(paths)
                for (i in paths.indices) {
                    val path = paths[i]
                    val color = colors[i]
                    drawPath(path, color)

                }
                for (line in lines) {
                    drawLine(
                        color_lines,
                        Offset((line.startX).toFloat(), line.startY.toFloat()),
                        Offset(line.endX.toFloat(), line.endY.toFloat())
                    )
                }
            }
//            Text(paths.firstOrNull() , color = Color.White )
        }
    }
}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL)
@Composable
fun DrawTreugolnik() {
//    val paths by remember { mutableStateOf(mutableListOf<Path>()) } // список для хранения всех путей
//    for (i in 0..8) {
//        val path = Path().apply {
//            moveTo(200f+i*100, 200f+i*400)
//            lineTo(300f+i*100, 400f+i*400)
//            lineTo(300f+i*100, 200f+i*400)
//            lineTo(500f+i*100, i*400f)
//            lineTo(400f+i*100, i*400f)
//            close()
//        }
//        paths.add(path)
//    }

    val gson = Gson()
    val context = LocalContext.current
    val inputStream: InputStream = context.assets.open("ИГиП_1.geojson")
    val size: Int = inputStream.available()
    val buffer = ByteArray(size)
    inputStream.read(buffer)
    val res = gson.fromJson(String(buffer), FeatureCollections::class.java)

    val paths by remember { mutableStateOf(mutableListOf<Path>()) } // список для хранения всех путей
    val texts by remember { mutableStateOf(mutableListOf<String>()) } // список для хранения всех путей
    val colors by remember { mutableStateOf(mutableListOf<Color>()) } // список для хранения всех путей
    val features = res.features
    features.forEach { feature ->
//        val color = Color("ac".toInt(16),"e5".toInt(16),"ff".toInt(16))
        var col = feature.properties.color
        val color = Color(
            (col[0].toString() + col[1].toString()).toInt(16),
            (col[2].toString() + col[3].toString()).toInt(16),
            (col[4].toString() + col[5].toString()).toInt(16),
            0xC0
        )
        var path = Path()
        if (feature.geometry.coordinates.isNotEmpty()) {
            if (feature.geometry.coordinates[0].isNotEmpty()) {
                path.moveTo(
                    feature.geometry.coordinates[0].first()[0].toFloat(),
                    feature.geometry.coordinates[0].first()[1].toFloat()
//                    0f,0f
                )
                for (i in 1 until feature.geometry.coordinates[0].size)
                    path.lineTo(
                        feature.geometry.coordinates[0][i][0].toFloat(),
                        feature.geometry.coordinates[0][i][1].toFloat()
                    )
//                path.addRect(Rect(Offset.Zero, Offset(400f, 400f)))
//                path.close()
                //            val path = Path()
                //            path.moveTo(200f, 200f)
                //            path.lineTo(300f, 400f)
                //            path.lineTo(400f, 200f)
                //            path.moveTo(200f, 200f)
                //            path.close()
                //            }
                if (color in colors) {
                    val ind = colors.indexOf(color)
                    paths[ind].addPath(path)
                } else {
                    paths.add(path)
                    colors.add(color)
                }
            }
        }
    }
    PolyShape(
        paths = paths,
        colors = colors
    )
}