class Graph {
    data class Vertex(val name: String) {
        val neighbors = mutableSetOf<Vertex>()
    }

    val line = mutableSetOf<String>()

    private val vertices = mutableMapOf<String, Vertex>()
    private operator fun get(name: String) = vertices[name] ?: throw IllegalArgumentException()
    fun addVertex(name: String) {
        vertices[name] = Vertex(name)
    }

    fun debugVertex() = vertices

    private fun connect(first: Vertex, second: Vertex) {
        first.neighbors.add(second)
        second.neighbors.add(first)
    }

    fun connect(first: String, second: String) = connect(this[first], this[second])
    fun bfs(start: String, finish: String) = bfs(this[start], this[finish])

    private fun bfs(start: Vertex, finish: Vertex): Int {
        val queue = ArrayDeque<Vertex>()
        queue.add(start)
        val visited = mutableMapOf(start to 0)
        val pr = mutableMapOf(start.name to "")

        while (queue.isNotEmpty()) {
            val next = queue.first()
            queue.remove(next)
            val distance = visited[next]!!
            if (next == finish) {
                println(visited)
                println(pr)
                line.clear()
                line.add(finish.name)
                var pred = finish.name
                var flg = true
                while (flg) {

                    pr[pred]?.let {
                        line.add(it)
                        pred = it
                        if (it == start.name)
                            flg = false
                    }
                }
                return distance
            }
            for (neighbor in next.neighbors) {
                if (neighbor in visited) continue
                visited[neighbor] = distance + 1
                pr[neighbor.name] = next.name
                queue.add(neighbor)
            }
        }
        return -1
    }
}