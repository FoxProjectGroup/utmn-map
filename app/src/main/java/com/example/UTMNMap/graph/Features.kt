import com.google.gson.annotations.Expose
import java.util.*

class Feature {
    @Expose
    val type = "Feature"

    @Expose
    var geometry = Geometry()

    @Expose
    val id = UUID.randomUUID()

    @Expose
    var properties = Properties()
}

class Properties {
    @Expose
    var level = 1

    @Expose
    var height = 0

    @Expose
    var base_height = 0

    @Expose
    var color = "grey"

    @Expose
    var name = UUID.randomUUID().toString()
}


