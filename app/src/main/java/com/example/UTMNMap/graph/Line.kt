import com.google.gson.annotations.Expose
import java.util.UUID
import kotlin.math.abs
import kotlin.math.sqrt

class Line(start: Piece, end: Piece) {

    @Expose
    var startX = 0.0

    @Expose
    var startY = 0.0

    @Expose
    var startId = UUID.randomUUID().toString()

    @Expose
    var endId = UUID.randomUUID().toString()

    @Expose
    var endX = 0.0

    @Expose
    var endY = 0.0

    @Expose
    val id: UUID = UUID.randomUUID()

    @Expose
    var wt = 0.0

    init {
        startX = start.posX
        startY = start.posY
        startId = start.id
        endX = end.posX
        endY = end.posY
        endId = end.id
        wt = sqrt(((abs(startX - endX) + abs(startY - endY)).toDouble()))
    }


}