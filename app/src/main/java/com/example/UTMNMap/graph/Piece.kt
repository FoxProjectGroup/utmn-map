import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.google.gson.annotations.Expose
import java.util.UUID

class Piece(X: Double, Y: Double) {
    var pos_x by mutableStateOf(0.0)
    var pos_y by mutableStateOf(0.0)

    @Expose
    var connect = false

    @Expose
    var posX: Double = 0.0

    @Expose
    var posY: Double = 0.0

    @Expose
    var id = UUID.randomUUID().toString()

    init {
        pos_x = X
        pos_y = Y
        posX = X
        posY = Y
    }


    fun import() {

        pos_x = posX
        pos_y = posY
    }

}