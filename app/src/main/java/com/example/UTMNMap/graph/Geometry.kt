import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.google.gson.annotations.Expose

data class FeatureCollections(
    @Expose val features: List<Feature> = listOf(),
    @Expose val type: String = ""
)

data class DataGrid(@Expose val pieces: List<Piece>, @Expose val lines: List<Line>)
class Geometry {
    var s by mutableStateOf(-1)

    var Coordinates = mutableStateListOf<List<Double>>()

    @Expose
    val type = "Polygon"

    @Expose
    var coordinates = emptyList<List<List<Double>>>()
    fun export() {
        val c = mutableStateListOf<List<Double>>()
        for (i in Coordinates) {
            c.add(listOf(i[0], i[1]))
        }
        c.add(listOf(Coordinates[0][0], Coordinates[0][1]))
        coordinates = listOf(c.toList())
    }

    fun import() {
        for (i in 0 until (coordinates[0].size - 1)) {
            Coordinates.add(coordinates[0][i])
        }
        export()
    }
}